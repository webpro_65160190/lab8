import { AppDataSource } from "./data-source";
import { Type } from "./entity/Type";

AppDataSource.initialize()
  .then(async () => {
    const typesRepositry = AppDataSource.getRepository(Type);
    await typesRepositry.clear();
    var type = new Type();
    type.id = 1;
    type.name = "drink";
    await typesRepositry.save(type);

    var type = new Type();
    type.id = 2;
    type.name = "bakery";
    await typesRepositry.save(type);

    var type = new Type();
    type.id = 3;
    type.name = "food";
    await typesRepositry.save(type);

    const types = await typesRepositry.find({ order: { id: "ASC" } });
    console.log(types);
  })
  .catch((error) => console.log(error));
