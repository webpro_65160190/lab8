import { create } from "domain";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from "typeorm";
import { Type } from "./Type";

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @CreateDateColumn()
  create: Date;

  @UpdateDateColumn()
  update: Date;

  @ManyToOne(() => Type, (type) => type.products)
  type: Type;
}
